

$( document ).ready(function() {

	precarga_home();
	funcionalidad();
	placeholer();
});


$(window).load(function(){
	
})



$( window ).resize(function(){
	var tmp = 250;
	console.log("holis");
	$("section section.contacto .content .sideleft").delay(tmp*1).animate({"margin-top":"0"},600);
	$("section section.contacto .content .sideright").delay(tmp*3).animate({"margin-top":"0"},600);
	

	if ($(window).width() > 1024) {
	   $("section.wrapper.contacto").css({"height":"100%","overflow":"hidden"},600);
	}
	else {
	   
	   $("section.wrapper.contacto").css({"height":"auto","overflow":"visible"},600);
	}
});



function precarga_home(){
	var imageload = {
		1 : 'images/fondo_textura.jpg',
	};
	var loader = new PxLoader();
	for( pos in imageload ){
		var pxImage = new PxLoaderImage( imageload[pos] );
		loader.add(pxImage);
	}
	loader.addProgressListener(function(e){
		var percentage = Math.ceil( ( e.completedCount / e.totalCount ) * 100 );
	});
	loader.addCompletionListener(function(){
		$('#loader').fadeOut("slow");
		animacion_inicio();
	});
	loader.start();
}



function animacion_inicio(){
	var tmp = 250;

	if (screen.width < 800) {
	    if(window.orientation == 0) /* Portrait*/ 
	    {
		    $("section.home .titulo").delay(tmp*3).animate({"width":"80%"},600);
	  	}
	  	else // Landscape
		 {
	    	$("section.home .titulo").delay(tmp*3).animate({"width":"60%"},600);
	  	}
	}
	else {

	   $("section.home .titulo").delay(tmp*3).animate({"width":"80%"},600);
	}
}


function animacion_arbol(){
	var tmp = 250;
	$("section.arbol .arbol_etiqueta").animate({opacity:1});
	$("section.arbol .arbol_etiqueta").animate({"width":"80.83%", "bottom":"21%", "marginLeft":"-40.5%"},600);
	$("section.arbol .zona_regalo_de_arbol").delay(tmp*1).animate({"bottom":"0"},600);
	$("section.arbol .zona_regalo_de_arbol img.sombra_de_arbol").delay(tmp*1).animate({"width":"100%", "marginTop":"0"},600);
	$("section.arbol .word.share").delay(tmp*4).animate({paddingTop:0},600);
	$("section.arbol .word.happy_holidays").delay(tmp*5).animate({paddingTop:0},600);
	$("section.arbol .word.people").delay(tmp*6).animate({paddingTop:0},600);
	$("section.arbol .word.friends").delay(tmp*7).animate({paddingTop:0},600);
	$("section.arbol .word.together").delay(tmp*8).animate({paddingTop:0},600);
	$("section.arbol .word.time").delay(tmp*9).animate({paddingTop:0},600);
	$("section.arbol .word.give").delay(tmp*10).animate({paddingTop:0},600);
	$("section.arbol .word.joy").delay(tmp*11).animate({paddingTop:0},600);
	$("section.arbol .word.peace").delay(tmp*12).animate({paddingTop:0},600);
	$("section.arbol .word.laughter").delay(tmp*13).animate({paddingTop:0},600);
	$("section.arbol .word.faith").delay(tmp*14).animate({paddingTop:0},600);
	$("section.arbol .word.humanity").delay(tmp*15).animate({paddingTop:0},600);
	$("section.arbol .word.girls").delay(tmp*16).animate({paddingTop:0},600);
	$("section.arbol .word.hope").delay(tmp*17).animate({paddingTop:0},600);
	$("section.arbol .word.warmth").delay(tmp*18).animate({paddingTop:0},600);
	$("section.arbol .word.care").delay(tmp*19).animate({paddingTop:0},600);
	$("section.arbol .word.iam").delay(tmp*20).animate({paddingTop:0},600);
	$("section.arbol .word.kind").delay(tmp*21).animate({paddingTop:0},600);
	$("section.arbol .word.life").delay(tmp*22).animate({paddingTop:0},600);
	$("section.arbol .word.happy").delay(tmp*23).animate({paddingTop:0},600);
	$("section.arbol .word.strength").delay(tmp*24).animate({paddingTop:0},600);
	$("section.arbol .word.world").delay(tmp*25).animate({paddingTop:0},600);
	$("section.arbol .word.good").delay(tmp*26).animate({paddingTop:0},600);
	$("section.arbol .word.family").delay(tmp*27).animate({paddingTop:0},600);
	$("section.arbol .word.new").delay(tmp*28).animate({paddingTop:0},600);
	$("section.arbol .word.year").delay(tmp*29).animate({paddingTop:0},600);
	$("section.arbol .word.love").delay(tmp*30).animate({paddingTop:0},600);

	$("section.arbol .word.estrella").delay(tmp*31).animate({paddingTop:0},600, function(){
		timer = setTimeout(function(){
			$("section.arbol .word.estrella").css({"overflow":"visible"});
			$("section.arbol .word.estrella img.estrella2").fadeIn("fast", function(){
				$("section.arbol .word.estrella img.estrella2").addClass("activo");	

				timer = setTimeout(function(){
					
					$("section.arbol .word.estrella img.estrella2").removeClass("activo");

					timer = setTimeout(function(){
						$("section.arbol .word.estrella img.estrella2").addClass("activo");

						timer = setTimeout(function(){
							$("section.arbol .word.estrella img.estrella2").removeClass("activo");

							timer = setTimeout(function(){
								$("section.arbol .word.estrella img.estrella2").addClass("activo");

								timer = setTimeout(function(){
									$("section.arbol .word.estrella img.estrella2").removeClass("activo");

									timer = setTimeout(function(){
										$("section.arbol .word.estrella img.estrella2").addClass("activo");

										timer = setTimeout(function(){
											animacion_holidays();
										}, 900);

									}, 900);
											
								}, 600);

							}, 900);

						}, 600);

					}, 900);	

				}, 600);			
			});
		}, 1);
	});
}

function animacion_holidays(){
	var time = false;
	var tmp = 250;
	$("section.arbol").delay(tmp*2).fadeOut();
	$("section.holidays").delay(tmp*5).fadeIn();
	if (screen.width > 1024) {
	    $("section.holidays .titulo").delay(tmp*5).animate({"margin-top":"9%"},600);
	}	
	
	$("section.holidays .texto").delay(tmp*8).fadeIn();
	$("section.holidays .logo_sansilvestre").delay(tmp*10).fadeIn();
	$("section.holidays .btn_share_card").delay(tmp*12).animate({"right":"5%"},600);
}

function animacion_holidays_2(){
	var time = false;
	var tmp = 250;
	$("section.holidays .titulo").delay(tmp*1).animate({"margin-top":"9%"},600);
	$("section.holidays .texto").delay(tmp*4).fadeIn();
	$("section.holidays .logo_sansilvestre").delay(tmp*6).fadeIn();
	$("section.holidays .btn_share_card").delay(tmp*8).animate({"right":"5%"},600);
}

function animacion_contacto(){
	var time = false;
	var tmp = 250;
	
	if (screen.width > 1024) {
	    $("section section.contacto .content .sideleft").delay(tmp*1).animate({"margin-top":"0"},600);
		$("section section.contacto .content .sideright").delay(tmp*3).animate({"margin-top":"0"},600);
	}
	
	
}

function contra_animacion_holidays(){
	var time = false;
	var tmp = 250;
	$("section.holidays .titulo").delay(tmp*1).animate({"margin-top":"-300px"},600);
	$("section.holidays .texto").delay(tmp*1).fadeOut();
	$("section.holidays .logo_sansilvestre").delay(tmp*1).fadeOut();
	$("section.holidays .btn_share_card").delay(tmp*1).css({"right":"-15%"},600);
}

function contra_animacion_contacto(){
	var time = false;
	var tmp = 250;
	$("section section.contacto .content .sideleft").delay(tmp*1).animate({"margin-top":"-99%"},600);
	$("section section.contacto .content .sideright").delay(tmp*1).animate({"margin-top":"-99%"},600);
	$("section.wrapper").css({"overflow":"hidden", "height":"auto"},600);
	$("section section.contacto").css({"position":"absolute"},600);
	$("section section.contacto .content").css({"position":"absolute"},600);
}








function layer_gracias(){
	
	$('.layer_gracias').fadeIn('slow', function(){
		var timer = false;
			
		timer = setTimeout(function(){
			$(".caja_imagen").css('visibility','visible');
			
		}, 100);
	});


	$(".caja_imagen").on({
		click: function(){
			$('.layer_gracias').fadeOut();
			$("form input, form textarea").val("");
		}
	})

	window.scrollTo(0, 0);

	
	if (screen.width < 1025) {
		$("section.wrapper").css({"overflow":"hidden", "height":"auto"},600);
	}
	else {
	   
	}
}



function funcionalidad(){
	var time = false;
	var tmp = 250;
	$("section.home .zona_regalo").on({
		click: function(){
			$("section.home").fadeOut("fast", "linear");
			$("section.arbol").delay(tmp*2).fadeIn("slow", "linear", function(){
				timer = setTimeout(function(){
					animacion_arbol();
				}, 300);
			});

			$("#cancion.mt-ie9")[0].play();
			$(".btn_play").fadeIn();
			$(".btn_stop").fadeIn()

			/*$("section.holidays .logo_sansilvestre span.nombre").fadeIn();*/
		}
	})

	$(".btn_play").on({
		click: function(){
			$("#cancion")[0].play();
		}
	})

	$(".btn_stop").on({
		click: function(){
			$("#cancion")[0].pause();
		}
	})


	$("section.holidays .btn_share_card").on({
		click: function(){
			$("section.holidays").fadeOut("fast", "linear");
			$("section.contacto").delay(tmp*2).fadeIn("slow", "linear", function(){
				timer = setTimeout(function(){
					animacion_contacto();
					contra_animacion_holidays();
				}, 100);
			});

			$("section.wrapper").css({"overflow":"visible", "height":"auto"},0);
			$("section section.contacto").css({"position":"relative", "paddingTop":"66px"},0);
			$("section section.contacto .content").css({"position":"relative"},0);

			$("section.wrapper").addClass("contacto");

			if (screen.width > 1024) {
				$("section.wrapper").css({"overflow":"hidden", "height":"100%"},0);
				$("section section.contacto").css({"position":"absolute", "paddingTop":"0"},0);
				$("section section.contacto .content").css({"position":"absolute"},0);
			}
			
		}
	})

	$("form .caja .btn_send").on({
		click: function(){
			layer_gracias();
		}
	})
}



function placeholer(){

    // Invoke the plugin
    $('input, textarea').placeholder();
    // That’s it, really.
    // Now display a message if the browser supports placeholder natively
    var html;
    if ($.fn.placeholder.input && $.fn.placeholder.textarea) {
        html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> and <code>textarea</code> elements.</strong> The plugin won’t run in this case, since it’s not needed. If you want to test the plugin, use an older browser ;)';
    } else if ($.fn.placeholder.input) {
        html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> elements, but not for <code>textarea</code> elements.</strong> The plugin will only do its thang on the <code>textarea</code>s.';
    }
    if (html) {
        $('<p class="note">' + html + '</p>').insertAfter('form');
    }
}



